import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import ApplicantNon from '../views/Applicant1/ApplicantNon.vue'
import Admin from '../views/Admin/Admin.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/applicantnon',
    name: 'ApplicantNon',
    component: ApplicantNon
  },
  {
    path: '/admin',
    name: 'Admin',
    component: Admin
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/applicant2',
    name: 'Applicant2',
    component: () =>
      import(
        /* webpackChunkName: "about" */ '../views/Applicant2/Applicant2.vue'
      )
  },
  {
    path: '/companies',
    name: 'companies',
    component: () => import('../views/Companies2/index.vue')

  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
