const companyService = {
  companyList: [
    {
      no: 1,
      id: '1001ac',
      name: 'Apple',
      tel: '1800-019-900',
      status: 'success'
    },
    {
      no: 2,
      id: '1002cs',
      name: 'SpaceX',
      tel: '0800-095-988',
      status: 'pending'
    }
  ],
  lastNo: 3,
  addCompany (company) {
    company.no = this.lastNo++
    this.companyList.push(company)
  },
  updateCompany (company) {
    const index = this.companyList.findIndex(item => item.no === company.no)
    this.companyList.splice(index, 1, company)
  },
  deleteCompany (company) {
    const index = this.companyList.findIndex(item => item.no === company.no)
    this.companyList.splice(index, 1)
  },
  getCompanies () {
    return [...this.companyList]
  }
}

export default companyService
