const adminService = {
  adminList: [
    {
      id: 1,
      first_name: 'Rujirada',
      last_name: 'Norasarn',
      gender: 'F',
      birthday: '1999-01-27',
      tel: '061-666-6666'
    },
    {
      id: 2,
      first_name: 'Kantapond',
      last_name: 'Aonraksa',
      gender: 'F',
      birthday: '2000-07-17',
      tel: '083-888-8888'
    }
  ],
  lastId: 3,
  addAdmin (admin) {
    admin.id = this.lastId++
    this.adminList.push(admin)
  },
  updateAdmin (admin) {
    const index = this.adminList.findIndex(item => item.id === admin.id)
    this.adminList.splice(index, 1, admin)
  },
  deleteAdmin (admin) {
    const index = this.adminList.findIndex(item => item.id === admin.id)
    this.adminList.splice(index, 1)
  },
  getAdmins () {
    return [...this.adminList]
  }
}

export default adminService
