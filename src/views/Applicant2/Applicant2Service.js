const applicant2Service = {
  applicantList: [
    {
      id: 1,
      firstName: 'Rujirada',
      lastName: 'Norasarn',
      gender: 'F',
      birthday: '1999-01-27',
      nationality: 'Thai',
      tel: '061-666-6666',
      address: 'Thai'
    },
    {
      id: 2,
      firstName: 'Kantapon',
      lastName: 'Aonraksa',
      gender: 'F',
      birthday: '2000-07-17',
      nationality: 'Thai',
      tel: '069-555-4566',
      address: 'Thai'
    }
  ],
  lastId: 3,
  addapplicant (applicant) {
    applicant.id = this.lastId++
    this.applicantList.push(applicant)
  },
  updateapplicant (applicant) {
    const index = this.applicantList.findIndex(item => item.id === applicant.id)
    this.applicantList.splice(index, 1, applicant)
  },
  deleteApplicant (applicant) {
    const index = this.applicantList.findIndex(item => item.id === applicant.id)
    this.applicantList.splice(index, 1)
  },
  getapplicant2 () {
    return [...this.applicantList]
  }
}

export default applicant2Service
